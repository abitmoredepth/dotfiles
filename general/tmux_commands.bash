_tmux_send_keys_this_session_fn () {
  for _pane in $(tmux list-panes -s -F '#I'); do
  tmux send-keys -t ${_pane} "$@" Enter
  done
}
