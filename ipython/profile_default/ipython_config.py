"""IPython config file."""

# get_config is injected into the global namespace whilst
# config files are loaded
c = get_config()

# Autoreload modules. See:
# https://github.com/ipython/ipython/issues/461
# http://stackoverflow.com/questions/5364050/reloading-submodules-in-ipython
# https://support.enthought.com/hc/en-us/articles/204469240
# # c.InteractiveShellApp.extensions = ['autoreload']
# # c.InteractiveShellApp.exec_lines = ['%autoreload 2']
c.TerminalInteractiveShell.editing_mode = 'vi'

# Only autocomplete attributes in __all__
# c.IPCompleter.limit_to__all__ = True

# Increase the figure size from the default 8 x 6 inches
c.InlineBackend.rc.update({'figure.figsize': (10, 5.5)})
