source $config_root/autojump/aliases.sh
source $config_root/autojump/exports.sh

if [ $_OS_TYPE = 'DARWIN' ]; then
  [ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh
fi
