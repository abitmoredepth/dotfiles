# vi: ft=zsh

# - General
export PATH=$HOME/bin:$PATH
export VAMPIRE=1  # nvim colorscheme
export EDITOR="nvim"
export PAGER=less
export LESS=-XR

# Set OS type so we know what tools we have available for use
if [ -z "$_OS_TYPE" ]
then
  # for now assuming LINUX is UBUNTU
  # and DARWIN is MOJAVE
  # for future use -- get macOS version - use the following:
  # defaults read loginwindow SystemVersionStampAsString
  uname | grep -qi linux
  is_linux=$?
  [ $is_linux -eq 0 ] && _OS_TYPE='LINUX'
  uname | grep -qi darwin
  is_darwin=$?
  [ $is_darwin -eq 0 ] && _OS_TYPE='DARWIN'
  export _OS_TYPE
fi

# Darwin gnutools/coreutils - gnu tools with same name
# as stock macos tool will be prefixed with g
[ $_OS_TYPE = 'DARWIN' ] && PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

# - ZPLUG
# location for plugins to go
export ZPLUG_HOME=/usr/local/opt/zplug

# - zsh-completion-generator
# where completion manager stores it's generated
# completion files
export GENCOMPL_FPATH=$HOME/.zsh/plugins/zsh_completion_generator
