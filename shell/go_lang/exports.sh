export GOPATH=$HOME/go  # User specific or system wide (/usr/local/go)?
export PATH=/usr/local/go/bin:$PATH  # recommended go install location
export PATH=$PATH:$GOPATH/bin
