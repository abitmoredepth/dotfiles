# All things common are sourced here
source $config_root/aliases.sh
source $config_root/exports.sh

source $config_root/python/init.sh
source $config_root/javascript/init.sh
source $config_root/rust/init.sh
source $config_root/go_lang/init.sh
source $config_root/nodejs/init.sh
source $config_root/autojump/init.sh
# keep this path last for now so pyenv shims aren't effected
# by python installs through brew and only if we're on MacOS
if [ $_OS_TYPE = 'DARWIN' ]; then
  source $config_root/homebrew/init.sh
fi
