source $config_root/python/powerline/aliases.sh
source $config_root/python/powerline/exports.sh

# -- Note: for some reason powerline wasn't working
# on python version 3.7.1


if [ -z $powerline_installed ]; then
  which -s powerline-daemon > /dev/null
  export powerline_installed=$([ $? -eq 0 ] && echo TRUE || echo FALSE)
fi

if [ ${powerline_installed} = 'TRUE' ]
then

  if [ -z $pl_py_ver ]; then

    if [ $_OS_TYPE = 'DARWIN' ]; then
      pl_python_path=$(readlink $(which powerline-daemon)|sed -n 's;^\(.*bin\).*;\1\/python;p')

    elif [ $_OS_TYPE = 'LINUX' ]; then
      pl_python_path=$(readlink -f $(which powerline-daemon)|sed -n 's;^\(.*bin\).*;\1\/python;p')

    else
      echo "Error -- couldn't figure out _OS_TYPE for pl_py_ver"
      echo "will not be able to initalize with zsh and tmux powerline"

    fi

    export pl_py_ver=$("$pl_python_path" -c "import sys; print('{}.{}'.format(sys.version_info.major, sys.version_info.minor))")

  fi

  # initilize powerline-status for zsh
  # TODO: is there a bash equivalent - might have to check SHELL and decide on what to run
  source ~/.local/venvs/powerline-status/lib/python${pl_py_ver}/site-packages/powerline/bindings/zsh/powerline.zsh

else
  echo "Missing 'powerline-status'... please install for shell prompt awesomeness"

fi
