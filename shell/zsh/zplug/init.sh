source $zsh_root/zplug/aliases.sh
source $zsh_root/zplug/exports.sh
source $ZPLUG_HOME/init.zsh

zplug "ael-code/zsh-colored-man-pages"
zplug "RobSis/zsh-completion-generator"
zplug "zsh-users/zsh-syntax-highlighting"
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-completions"
#zplug "jimeh/zsh-peco-history"  # need to get this working

if ! zplug check; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

zplug load
